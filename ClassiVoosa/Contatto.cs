﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoosaEye
{
    public class Contatto : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private string nome;

        public string Nome
        {
            get
            {
                return nome;
            }
        }

        private string numero;

        public string Numero
        {
            get
            {
                return numero;
            }
        }

        private double latitudine;

        public double Latitudine
        {
            get
            {
                return latitudine;
            }
            set
            {
                latitudine = value;
                NotifyPropertyChanged("Latitudine");
            }
        }

        private double longitudine;

        public double Longitudine
        {
            get
            {
                return longitudine;
            }
            set
            {
                longitudine = value;
                NotifyPropertyChanged("Longitudine");
            }
        }

        private DateTime ultimaRilevazione;

        public string UltimaRilevazione
        {
            get
            {
                DateTime system = System.DateTime.MinValue;
                if (ultimaRilevazione.Year > system.Year)
                    return ultimaRilevazione.ToString("HH:mm - dd/MM/yyyy");
                else
                    return "N/A";
            }
            set
            {
                ultimaRilevazione = DateTime.ParseExact(value, "HH:mm - dd/MM/yyyy", CultureInfo.InvariantCulture);
                NotifyPropertyChanged("UltimaRilevazione");
            }
        }

        private double latPunto;

        public double LatPunto
        {
            get
            {
                return latPunto;
            }
            set
            {
                latPunto = value;
                NotifyPropertyChanged("LatPunto");
            }
        }

        private double lonPunto;

        public double LonPunto
        {
            get
            {
                return lonPunto;
            }
            set
            {
                lonPunto = value;
                NotifyPropertyChanged("LonPunto");
            }
        }

        private long raggio;

        public long Raggio
        {
            get
            {
                return raggio == 0 ? 50 : raggio;
            }
            set
            {
                raggio = value;
                NotifyPropertyChanged("Raggio");
            }
        }

        private bool inDB;

        public bool InDB
        {
            get
            {
                return inDB;
            }

            set
            {
                inDB = value;
                NotifyPropertyChanged("InDB");
            }
        }

        public Contatto(string nome, string numero, bool forDb)
        {
            this.nome = nome;
            this.numero = numero;
            InDB = forDb;
        }

        public Contatto(string nome, string numero, double lat, double lng, string rilevazione)
        {
            this.nome = nome;
            this.numero = numero;
            this.latitudine = lat;
            this.longitudine = lng;
            UltimaRilevazione = rilevazione;
            InDB = true;
        }

        public Contatto(string nome, string numero, double lat, double lng, string rilevazione, double latpunto, double lonpunto, long raggioAzione)
        {
            this.nome = nome;
            this.numero = numero;
            this.latitudine = lat;
            this.longitudine = lng;
            UltimaRilevazione = rilevazione;
            InDB = true;
            LatPunto = latpunto;
            LonPunto = lonpunto;
            Raggio = raggioAzione;
        }

        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
