﻿using SQLitePCL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoosaEye
{
    public class DatabaseVoosa
    {
        static string DB_NAME = "VoosaDB.db";
        static string TABLE_CONTATTI = "contatti";
        static string ID_CONTATTO = "id";
        static string NOME_CONTATTO = "nome";
        static string NUMERO_CONTATTO = "numero";
        static string LAT_CONTATTO = "latitudine";
        static string LON_CONTATTO = "longitudine";
        static string DATA_CONTATTO = "rilevazione";
        static string LAT_PUNTO = "latpunto";
        static string LON_PUNTO = "lonpunto";
        static string PRECISIONE_PUNTO = "raggioazione";

        static string DELETE_TABLE = @"DROP TABLE " + TABLE_CONTATTI;

        static string CREATE_TABLE_CONTATTI = @"CREATE TABLE IF NOT EXISTS " + TABLE_CONTATTI + " ("
            + ID_CONTATTO + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NOME_CONTATTO + " TEXT, " + NUMERO_CONTATTO + " TEXT, "
            + LAT_CONTATTO + " DOUBLE, " + LON_CONTATTO + " DOUBLE, "
            + DATA_CONTATTO + " TEXT, " + LAT_PUNTO + " DOUBLE, "
            + LON_PUNTO + " DOUBLE, " + PRECISIONE_PUNTO + " INTEGER)";

        static DatabaseVoosa db;
        SQLiteConnection conn;

        private DatabaseVoosa() {}

        public static DatabaseVoosa ottieniIstanza()
        {
            if (db == null)
            {
                db = new DatabaseVoosa();
            }

            return db;
        }

        public void CreaTabella()
        {
            string errore = "";

            if (conn == null)
            {
                try
                {
                    conn = new SQLiteConnection(DB_NAME);
                }
                catch (Exception ex) { errore = ex.Message; }
            }

            try
            {
                /*ISQLiteStatement eliminazione = conn.Prepare(DELETE_TABLE);
                SQLiteResult result1 = eliminazione.Step();
                eliminazione.Dispose();*/

                ISQLiteStatement creazione = conn.Prepare(CREATE_TABLE_CONTATTI);
                SQLiteResult resutl = creazione.Step();
                creazione.Dispose();
            }
            catch (Exception ex)
            {
                errore = ex.Message;
                int i = 0;
            }

            ChiudiConnessione();
        }

        private void CreaConnessione()
        {
            if (conn == null)
            {
                conn = new SQLiteConnection(DB_NAME);
            }
        }

        private void ChiudiConnessione()
        {
            if (conn != null)
            {
                conn.Dispose();
                conn = null;
            }
        }

        public List<Contatto> RecuperaContatti()
        {
            List<Contatto> esistenti = new List<Contatto>();
            string errore = "";

            CreaConnessione();
            try
            {
                ISQLiteStatement ricerca = conn.Prepare(@"SELECT * FROM " + TABLE_CONTATTI + " ORDER BY " + NOME_CONTATTO);

                while (ricerca.Step() == SQLiteResult.ROW)
                {
                    if (ricerca[3] == null)
                        esistenti.Add(new Contatto((string)ricerca[1], (string)ricerca[2], true));
                    else if (ricerca[6] == null)
                        esistenti.Add(new Contatto((string)ricerca[1], (string)ricerca[2], (double)ricerca[3], (double)ricerca[4], (string)ricerca[5]));
                    else
                        esistenti.Add(new Contatto((string)ricerca[1], (string)ricerca[2], (double)ricerca[3], (double)ricerca[4], (string)ricerca[5], (double)ricerca[6], (double)ricerca[7], (long)ricerca[8]));
                }
            }
            catch (Exception ex)
            {
                errore = ex.Message;
                int i = 0;
            }

            ChiudiConnessione();
            return esistenti;
        }

        public bool AggiungiContatto(Contatto contatto)
        {
            bool esito = false;
            string exeption = "";
            CreaConnessione();

            try
            {
                ISQLiteStatement inserimento = conn.Prepare(@"INSERT INTO " + TABLE_CONTATTI +
                "(" + NOME_CONTATTO + "," + NUMERO_CONTATTO + ") VALUES(?,?)");

                inserimento.Bind(1, contatto.Nome);
                inserimento.Bind(2, contatto.Numero);

                SQLiteResult result = inserimento.Step();

                if (result == SQLiteResult.DONE)
                {
                    esito = true;
                }
                inserimento.Dispose();
            }
            catch (Exception ex) 
            { 
                exeption = ex.Message;
                int i = 0; 
            }
            
            ChiudiConnessione();
            return esito;
        }

        public bool AggiornaContatto(Contatto contatto)
        {
            bool esito = false;
            string exeption = "";
            CreaConnessione();

            try
            {
                ISQLiteStatement aggiornamento = conn.Prepare(@"UPDATE " + TABLE_CONTATTI + " SET " +
                    LAT_CONTATTO + " = ?, " + LON_CONTATTO + " = ?, " + DATA_CONTATTO + " = ? " +
                "WHERE " + NUMERO_CONTATTO + " = ?");

                aggiornamento.Bind(1, contatto.Latitudine);
                aggiornamento.Bind(2, contatto.Longitudine);
                aggiornamento.Bind(3, contatto.UltimaRilevazione);
                aggiornamento.Bind(4, contatto.Numero);

                SQLiteResult result = aggiornamento.Step();

                if (result == SQLiteResult.DONE)
                {
                    esito = true;
                }
                aggiornamento.Dispose();
            }
            catch (Exception ex)
            {
                exeption = ex.Message;
                int i = 0;
            }

            ChiudiConnessione();
            return esito;
        }

        public bool AggiornaPuntoInteresseContatto(Contatto contatto)
        {
            bool esito = false;
            string exeption = "";

            CreaConnessione();

            try
            {
                ISQLiteStatement interesse = conn.Prepare(@"UPDATE " + TABLE_CONTATTI + " SET " +
                    LAT_PUNTO + " = ?, " + LON_PUNTO + " = ?, " + PRECISIONE_PUNTO + " = ? " +
                    "WHERE " + NUMERO_CONTATTO + " = ?");

                interesse.Bind(1, contatto.LatPunto);
                interesse.Bind(2, contatto.LonPunto);
                interesse.Bind(3, contatto.Raggio);
                interesse.Bind(4, contatto.Numero);

                SQLiteResult result = interesse.Step();

                if (result == SQLiteResult.DONE)
                {
                    esito = true;
                }
                interesse.Dispose();
            }
            catch (Exception ex)
            {
                exeption = ex.Message;
                int i = 0;
            }

            ChiudiConnessione();

            return esito;
        }

        public bool EliminaContatto(Contatto contatto)
        {
            bool esito = false;
            string exeption = "";
            CreaConnessione();

            try
            {
                ISQLiteStatement eliminazione = conn.Prepare(@"DELETE FROM " + TABLE_CONTATTI +
                " WHERE " + NOME_CONTATTO + " = ?");

                eliminazione.Bind(1, contatto.Nome);

                SQLiteResult result = eliminazione.Step();

                if (result == SQLiteResult.DONE)
                {
                    esito = true;
                }
                eliminazione.Dispose();
            }
            catch (Exception ex)
            {
                exeption = ex.Message;
                int i = 0;
            }

            ChiudiConnessione();
            return esito;
        }
    }
}
