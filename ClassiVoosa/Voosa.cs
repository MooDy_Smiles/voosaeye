﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Storage;

namespace VoosaEye
{
    public sealed class Voosa
    {
        private static String NumeroAccount = "numero_account";
        private static String UsoPosizione = "uso_posizione";
        private static String IntervalloTask = "intervallo_task";
        private static String taskName = "OfficialTask";
        private static String ServerUri = "http://192.168.0.49:80/voosaeye/";
        private static String AggiungiAggiornaAccount = "inserisci_aggiorna_account.php";
        private static String EliminaAccount = "elimina_account.php";
        private static String ControlloLista = "controllo_lista.php";
        private static String ControllaPosizione = "controllo_posizione.php";

        public static bool esisteAccount()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            return localSettings.Values.ContainsKey(NumeroAccount) && !localSettings.Values[NumeroAccount].Equals("");
        }

        public static String getNomeTask()
        {
            return taskName;
        }

        // Getter del tag delle LocalSettings
        public static String getNumeroAccount()
        {
            return NumeroAccount;
        }

        public static String getUsoPosizione()
        {
            return UsoPosizione;
        }

        public static String getIntervalloTask()
        {
            return IntervalloTask;
        }

        // Getter del percorso al server
        public static String getServerUri()
        {
            return ServerUri;
        }

        // Getter della pagina di inserimento/modifica account
        public static String getAggiungiAggiornaAccount()
        {
            return AggiungiAggiornaAccount;
        }

        // Getter della pagina di eliminazione dell'account
        public static String getEliminaAccount()
        {
            return EliminaAccount;
        }

        public static string getControlloLista()
        {
            return ControlloLista;
        }

        public static string getControllaPosizione()
        {
            return ControllaPosizione;
        }

        public static bool controllaGPS()
        {
            Geolocator locator = new Geolocator();
            if (locator.LocationStatus == PositionStatus.Disabled)
            {
                return false;
            }

            return true;
        }

        public static bool nellaZona(Contatto contatto)
        {
            double dist = DistanceTo(contatto.Latitudine, contatto.Longitudine, contatto.LatPunto, contatto.LonPunto);

            if (dist <= contatto.Raggio)
                return true;

            return false;
        }

        private static double DistanceTo(double lat1, double lon1, double lat2, double lon2)
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            return dist * 1609.34;
        }
    }
}
