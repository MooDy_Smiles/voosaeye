<?php
	
	require_once 'DBConnection.php';
	require_once 'DBManager.php';
	require_once 'Constants.php';

	$conn = DBConnection::getConnection();
	
	if(isset($_GET[Constants::NUMBER])) $number = $_GET[Constants::NUMBER];
	
	// Check connection
	if ($conn->connect_error) {
		die("Connessione fallita: " . $conn->connect_error);
	}
	
	if(isset($_GET[Constants::NUMBER])){
		if(!DBManager::checkAccountTableExistence($conn)){
			
			die("Tabella non esistente");
		} else {
			
			if(DBManager::checkAccountExistence($conn, $number) === TRUE){
				
				if(DBManager::removeAccount($conn, $number) === TRUE){
					echo "Eliminazione avvenuta";
				} else {
					die("Eliminazione fallita");
				}
			} else {
				
				die("Account inesistente, emilinazione impossibile");
			}
		}
	} else {
		
		die("Dati mancanti");
	}
	
?>