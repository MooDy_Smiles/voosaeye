﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class CreaModificaAccount : Page
    {

        private string pat = @"[0-9]{10}";
        private ApplicationDataContainer account;

        public CreaModificaAccount()
        {
            this.InitializeComponent();
            account = ApplicationData.Current.LocalSettings;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!Voosa.esisteAccount())
            {
                eliminaAccount.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
            else
            {
                accountNumber.Text = account.Values[Voosa.getNumeroAccount()].ToString();
                accountNumber.IsReadOnly = true;
                confermaAccount.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Frame.BackStack.Remove(this.Frame.BackStack.LastOrDefault());
        }

        private async void confermaDati(object sender, RoutedEventArgs e)
        {
            confermaAccount.IsEnabled = false;
            accountNumber.IsEnabled = false;
            LoadingRing.IsActive = true;
            LoadingRing.Visibility = Visibility.Visible;

            if (accountNumber.Text.Equals("") || !controlloRegex(accountNumber.Text))
            {
                MessageDialog msg = new MessageDialog("Campo numero contatto non valido.");
                await msg.ShowAsync();
            }
            else
            {
                HttpClient aggiungi = new HttpClient();

                try
                {
                    HttpResponseMessage msg = await aggiungi.GetAsync(new Uri(Voosa.getServerUri() + Voosa.getAggiungiAggiornaAccount() + "?numero=" + accountNumber.Text));
                    if (msg.IsSuccessStatusCode)
                    {
                        String risposta = await msg.Content.ReadAsStringAsync();

                        if (risposta.Equals("Account inserito"))
                        {
                            App.notificaUtente("Account creato");
                            account.Values[Voosa.getNumeroAccount()] = accountNumber.Text;
                            Frame.Navigate(typeof(ListaContatti));
                        }
                        else if (risposta.Equals("Nessun dato da aggiornare"))
                        {
                            App.notificaUtente("Account recuperato");
                            account.Values[Voosa.getNumeroAccount()] = accountNumber.Text;
                            Frame.Navigate(typeof(ListaContatti));
                        }
                        else
                        {
                            App.notificaUtente("Impossibile creare account");
                        }
                        
                    }
                    else
                    {
                        App.notificaUtente("Errore connessione al server.");
                    }
                }
                catch (Exception ex)
                {
                    App.notificaUtente(ex.Message);
                }
            }

            LoadingRing.IsActive = false;
            LoadingRing.Visibility = Visibility.Collapsed;
            accountNumber.IsEnabled = true;
            confermaAccount.IsEnabled = true;
        }

        private bool controlloRegex(String valore)
        {
            Regex tester = new Regex(pat);

            return tester.Match(valore).Success;
        }

        private async void CancellaAccount(object sender, RoutedEventArgs e)
        {
            HttpClient elimina = new HttpClient();

            try
            {
                HttpResponseMessage response = await elimina.GetAsync(Voosa.getServerUri() + Voosa.getEliminaAccount() + "?numero=" + account.Values[Voosa.getNumeroAccount()].ToString());
                if (response.IsSuccessStatusCode)
                {
                    String risposta = await response.Content.ReadAsStringAsync();

                    if (risposta.Equals("Eliminazione avvenuta"))
                    {
                        App.notificaUtente("Account eliminato con successo");
                        account.Values.Remove(Voosa.getNumeroAccount());
                        this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
                        Frame.Navigate(typeof(CreaModificaAccount));
                    }
                    else
                    {
                        App.notificaUtente("Errore nell'eliminazione.\n" + risposta);
                    }
                }
                else
                {
                    App.notificaUtente("Errore eliminazione account nel server.");
                }
            }
            catch (Exception ex)
            {
                App.notificaUtente("Errore: " + ex.Message);
            }
        }
    }
}
