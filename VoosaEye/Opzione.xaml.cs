﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Background;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class Opzione : Page
    {
        ApplicationDataContainer localSettings;

        public Opzione()
        {
            this.InitializeComponent();

            inizializzaComponenti();
        }

        private void inizializzaComponenti()
        {
            localSettings = ApplicationData.Current.LocalSettings;

            if (localSettings.Values.ContainsKey(Voosa.getUsoPosizione()))
            {
                posizione.IsOn = (bool)localSettings.Values[Voosa.getUsoPosizione()];
            }
            else
            {
                posizione.IsOn = true;
            }

            cmbIntervallo.Items.Add("15 minuti");
            cmbIntervallo.Items.Add("30 minuti");
            cmbIntervallo.Items.Add("45 minuti");
            cmbIntervallo.Items.Add("60 minuti");
            cmbIntervallo.SelectedIndex = 0;

            gestisciTest.Visibility = Visibility.Collapsed;

            /*if (App.checkTask())
                gestisciTest.Content = "Elimina Test Task";
            else
                gestisciTest.Content = "Registra Test Task";*/
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Frame.BackStack.Remove(this.Frame.BackStack.LastOrDefault());
        }

        private void SalvaOpzioni(object sender, RoutedEventArgs e)
        {
            int intervallo = cmbIntervallo.SelectedIndex + 1 * 15;

            if (localSettings.Values.ContainsKey(Voosa.getIntervalloTask()))
            {
                if (Int32.Parse(localSettings.Values[Voosa.getIntervalloTask()].ToString()) != intervallo)
                {
                    localSettings.Values[Voosa.getIntervalloTask()] = intervallo.ToString();
                    App.ricaricaTask();
                }
            }

            localSettings.Values[Voosa.getUsoPosizione()] = posizione.IsOn;

            App.notificaUtente("Impostazioni salvate.");

            Frame.GoBack();
        }

        private void gestisciTestTask(object sender, RoutedEventArgs e)
        {
            if (App.checkTask())
            {
                App.eliminaTask();
                gestisciTest.Content = "Registra Test Task";
            }
            else
            {
                App.registraTask();
                gestisciTest.Content = "Elimina Test Task";
            }
        }
    }
}
