<?php
	
	require_once 'DBConnection.php';
	require_once 'DBManager.php';
	require_once 'Constants.php';

	$conn = DBConnection::getConnection();
	
	if(isset($_GET[Constants::NUMBER])) $number = $_GET[Constants::NUMBER];
	if(isset($_GET[Constants::LAT])) $lat = $_GET[Constants::LAT];
	if(isset($_GET[Constants::LNG])) $lng = $_GET[Constants::LNG];

	// Check connection
	if ($conn->connect_error) {
		die("Connessione fallita: " . $conn->connect_error);
	}
	
	echo "<br>$number -> ". floatval($lat) ." : $lng<br>";
	
	if(DBManager::checkAccountTableExistence($conn)){
		
		echo "<br>Tabella esistente<br>";
	} else {
		$created = DBManager::createAccountTable($conn);
		
		if($created === TRUE)
			echo "<br>Tabella ". DBConnection::DB_ACCOUNTTABLE ." creata<br>";
		else
			echo "<br>Tabella ". DBConnection::DB_ACCOUNTTABLE ." non creata<br>";
	}
	
	if(DBManager::checkAccountTableExistence($conn)){
		
		if(DBManager::checkAccountExistence($conn, $number)){
			echo "<br><span>ESISTENTE</span><br>";
			
			if(isset($_GET[Constants::LAT]) && isset($_GET[Constants::LNG])){
				if(DBManager::updateCoordinates($conn, $number, $lat, $lng)){
					echo "<br>Dati aggiornati<br>";
				}
			}
		} else {
			
			if(isset($_GET[Constants::LAT]) && isset($_GET[Constants::LNG])){
				if(DBManager::addAccount($conn, $number, $lat, $lng)){
					echo "<br><span>TOTALE INSERITO</span><br>";
				}
			} else {
				if(DBManager::addPartialAccount($conn, $number)){
					echo "<br><span>PARZIALE INSERITO</span><br>";
				}
			}
		}
	}

?>