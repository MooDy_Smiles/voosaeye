<?php

/*$servername = "localhost";
$username = "root";
$password = "";
$dbname = "test";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);*/

require_once 'DBConnection.php';
require_once 'DBManager.php';

$conn = DBConnection::getConnection();

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully<br>";

$sql = "SELECT COLONNA1, COLONNA2 FROM prova";
$result = DBManager::executeQuery($conn, $sql);
//$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<h3>id: " . $row["COLONNA1"]. " - Value: " . $row["COLONNA2"]."</h3>";
    }
} else {
    echo "<h2>0 results</h2>";
}
DBConnection::releaseConnection($conn);
?>