﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Contacts;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class ListaContatti : Page
    {
        //private static List<Contatto> contattiValidi;
        private static ObservableCollection<Contatto> contattiValidi;
        private static List<Contatto> rubrica;

        public ListaContatti()
        {
            this.InitializeComponent();
            //this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            progressRing.IsActive = true;

            contattiValidi = new ObservableCollection<Contatto>();
            rubrica = new List<Contatto>();

            caricaContattiRubrica();

            progressRing.IsActive = false;
            caricaLabel.Visibility = Visibility.Collapsed;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            /*if (e.NavigationMode == NavigationMode.Back)
            {
                this.NavigationCacheMode = NavigationCacheMode.Disabled;

            }*/
        }

        private void GestisciAccount(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(CreaModificaAccount));
        }

        private void controlloLista()
        {
            List<KeyValuePair<string, string>> controllo = new List<KeyValuePair<string, string>>();

            int i = 0;

            foreach (Contatto contatto in rubrica){
                controllo.Add(new KeyValuePair<string, string>(i.ToString(), contatto.Numero));
                i++;
            }

            if (controllo.Count > 0)
            {
                controlloContattiServer(controllo);
            }
        }

        private async void controlloContattiServer(List<KeyValuePair<string, string>> form)
        {
            if (App.isConnectionAvailable())
            {
                HttpClient richiesta = new HttpClient(new HttpClientHandler());

                try
                {
                    HttpResponseMessage risposta = await richiesta.PostAsync(new Uri(Voosa.getServerUri() + Voosa.getControlloLista()), new FormUrlEncodedContent(form));

                    if (risposta.IsSuccessStatusCode)
                    {
                        string array = await risposta.Content.ReadAsStringAsync();

                        //ottengo le posizioni dei contatti che hanno attualmente l'app installata
                        //utilizzi le posizioni per creare la lista da mostrare a video
                        string[] posizioni = array.Split(';');

                        foreach (string posizione in posizioni)
                        {
                            contattiValidi.Add(rubrica.ElementAt(Int32.Parse(posizione)));
                        }

                        //eseguo controllo con dati presenti in DB
                        caricaDaDatabase();

                        ListaContattiUtente.DataContext = contattiValidi;
                    }
                    else
                    {
                        App.notificaUtente("Impossibile eseguire controllo al server.\nControllare rete o riprovare più tardi.");
                    }
                }
                catch (Exception ex)
                {
                    App.notificaUtente("Errore nel controllo dei contatti: " + ex.Message);
                }
            }
            else
            {
                App.notificaUtente("Impossibile recuperare dati, rete non accessibile.");
            }
            
        }

        private void mostraContatto(object sender, ItemClickEventArgs e)
        {
            try
            {
                Frame.Navigate(typeof(VisualizzaContatto), e.ClickedItem);
            }
            catch (Exception ex)
            {
                App.notificaUtente("Errore: " + ex.Message);
            }
        }

        private void GestisciOpzioni(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Opzione));
        }

        private async void caricaContattiRubrica()
        {
            ContactStore contactStore = await ContactManager.RequestStoreAsync();

            IReadOnlyList<Contact> contacts = null;
            // Find all contacts
            contacts = await contactStore.FindContactsAsync();

            foreach (Contact item in contacts)
            {
                rubrica.Add(new Contatto(item.DisplayName, item.Phones[0].Number, false));
            }

            controlloLista();
        }

        private void caricaDaDatabase()
        {
            List<Contatto> controllo = DatabaseVoosa.ottieniIstanza().RecuperaContatti();
            List<int> validatiNot = new List<int>();

            if (controllo.Count > 0)
            {
                foreach (Contatto inControllo in controllo)
                {
                    foreach (Contatto valido in contattiValidi)
                    {
                        if (inControllo.Nome.Equals(valido.Nome))
                        {
                            int index = contattiValidi.IndexOf(valido);
                            contattiValidi.RemoveAt(index);
                            contattiValidi.Insert(index, inControllo);
                            break;
                        }
                    }

                    //tengo traccia degli indici dei contatti che non vengono visualizzati
                    //poiché hanno disinstallato l'applicazione
                    if (contattiValidi.IndexOf(inControllo) == -1)
                        validatiNot.Add(controllo.IndexOf(inControllo));
                }
            }

            //elimino dal database i contatti che hanno disinstallato l'app e che seguivo
            foreach (int index in validatiNot)
                DatabaseVoosa.ottieniIstanza().EliminaContatto(controllo.ElementAt(index));
        }
    }
}
