﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class ProvaRecuperoMassivo : Page
    {
        private List<Contatto> pedinare;
        private List<KeyValuePair<string, string>> form;

        public ProvaRecuperoMassivo()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            pedinare = DatabaseVoosa.ottieniIstanza().RecuperaContatti();

            preparaForm();
        }

        private async void RecuperoMassivo(object sender, RoutedEventArgs e)
        {
            string array = "";

            if (App.isConnectionAvailable())
            {
                HttpClient richiesta = new HttpClient(new HttpClientHandler());

                try
                {
                    HttpResponseMessage risposta = await richiesta.PostAsync(new Uri(Voosa.getServerUri() + Voosa.getControllaPosizione()), new FormUrlEncodedContent(form));
                    if (risposta.IsSuccessStatusCode)
                    {
                        array = await risposta.Content.ReadAsStringAsync();

                        JsonObject json = JsonObject.Parse(array);

                        foreach (Contatto contatto in pedinare)
                        {
                            JsonArray jarray = json.GetNamedArray(contatto.Numero);

                            foreach (var item in jarray)
                            {
                                var itemObj = item.GetObject();

                                string data = itemObj.GetNamedString("when");

                                if (!data.Equals("null") && !contatto.UltimaRilevazione.Equals(data))
                                {
                                    string lati = itemObj.GetNamedString("lat").Replace(".", ",");
                                    string longi = itemObj.GetNamedString("lng").Replace(".", ",");
                                    double lat = double.Parse(lati);
                                    double lng = double.Parse(longi);

                                    contatto.Latitudine = lat;
                                    contatto.Longitudine = lng;
                                    contatto.UltimaRilevazione = data;

                                    //TODO check sulla posizione del punto di interesse
                                    if (Voosa.nellaZona(contatto))
                                    {
                                        ToastTemplateType toastType = ToastTemplateType.ToastText02;

                                        XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

                                        XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
                                        toastTextElement[0].AppendChild(toastXml.CreateTextNode(contatto.Nome));
                                        toastTextElement[1].AppendChild(toastXml.CreateTextNode("Ha raggiunto la zona di interesse alle " + contatto.UltimaRilevazione));

                                        IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                                        ((XmlElement)toastNode).SetAttribute("duration", "long");

                                        ToastNotification toast = new ToastNotification(toastXml);
                                        ToastNotificationManager.CreateToastNotifier().Show(toast); 
                                    }

                                    bool ok = DatabaseVoosa.ottieniIstanza().AggiornaContatto(contatto);
                                    int i = 0;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    array = ex.Message;
                }
            }

            txtJSON.Text = array;
        }

        private void preparaForm()
        {
            form = new List<KeyValuePair<string, string>>();

            int i = 1;

            foreach (Contatto contatto in pedinare)
            {
                form.Add(new KeyValuePair<string, string>(i.ToString(), contatto.Numero));
                i++;
            }
        }
    }
}
