﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class PuntoInteresse : Page
    {
        private const int MappaAltezza = 560;
        private MapIcon punto;
        private Contatto contatto;
        private List<int> distanze;

        public PuntoInteresse()
        {
            this.InitializeComponent();

            distanze = new List<int>();

            distanze.Add(50);
            distanze.Add(100);
            distanze.Add(150);
            distanze.Add(200);
            distanze.Add(250);
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            contatto = e.Parameter as Contatto;

            btnPrecisione.DataContext = contatto;

            disegnaMappa();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            //this.Frame.BackStack.Remove(this.Frame.BackStack.LastOrDefault());
        }

        private void SalvaPunto(object sender, RoutedEventArgs e)
        {
            if (contatto.LatPunto != 0)
            {
                if (!DatabaseVoosa.ottieniIstanza().AggiornaPuntoInteresseContatto(contatto))
                    App.notificaUtente("È stato impossibile aggiornare il punto di interesse nel database.");
                else
                {
                    App.notificaUtente("Punto di interesse aggiornato.");
                }
                    
            }
            else
            {
                App.notificaUtente("Nessun punto sulla mappa selezionato.");
            }

            //Frame.Navigate(typeof(VisualizzaContatto), contatto);
        }

        private void PinPunto(MapControl sender, MapInputEventArgs args)
        {
            contatto.LatPunto = args.Location.Position.Latitude;
            contatto.LonPunto = args.Location.Position.Longitude;

            disegnaMappa();
        }

        private void disegnaMappa()
        {
            if (punto != null)
                mappaPunto.MapElements.Remove(punto);
            else
                punto = new MapIcon();
            
            BasicGeoposition basic = new BasicGeoposition();
            
            if (contatto.LatPunto == 0)
            {                
                basic.Latitude = 44.4355049;
                basic.Longitude = 10.9767865;
                mappaPunto.Center = new Geopoint(basic);
            }
            else
            {
                basic.Latitude = contatto.LatPunto;
                basic.Longitude = contatto.LonPunto;
                mappaPunto.Center = new Geopoint(basic);
                mappaPunto.ZoomLevel = 15;
                punto.Title = "Interesse";
                punto.Location = new Geopoint(basic);
                punto.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/punto_pin.png"));
                punto.NormalizedAnchorPoint = new Point(0.5, 0.9);
                mappaPunto.MapElements.Add(punto);
            }         
        }

        private async void MostraFlyout(object sender, RoutedEventArgs e)
        {
            var flyout = new ListPickerFlyout { ItemsSource = distanze };
            await flyout.ShowAtAsync(btnPrecisione);

            if (flyout.SelectedItem == null) return;

            var itemPicked = flyout.SelectedValue;

            contatto.Raggio = (int)itemPicked;
        }
    }
}
