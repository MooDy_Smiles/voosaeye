﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Connectivity;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkId=391641

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: preparare la pagina da visualizzare qui.

            // TODO: se l'applicazione contiene più pagine, assicurarsi che si stia
            // gestendo il pulsante Indietro dell'hardware effettuando la registrazione per
            // l'evento Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Se si utilizza l'elemento NavigationHelper fornito da alcuni modelli,
            // questo evento viene gestito automaticamente.

            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            if (!localSettings.Values.ContainsKey(Voosa.getNumeroAccount()))
            {
                visualizza.Text = "No dati in " + Voosa.getNumeroAccount() + ".";
            }
            else
            {
                visualizza.Text = localSettings.Values[Voosa.getNumeroAccount()].ToString();
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.Frame.BackStack.Remove(this.Frame.BackStack.LastOrDefault());
        }

        private async void InviaDati(object sender, RoutedEventArgs e)
        {
            if (!nome.Text.Equals(""))
            {
                if (App.isConnectionAvailable())
                {
                    HttpClient client = new HttpClient();
                    string roba = "";

                    try
                    {
                        HttpResponseMessage msg = await client.GetAsync(new Uri("http://localhost/voosaeye/prova.php?nome=" + nome.Text));
                        if (msg.IsSuccessStatusCode)
                        {
                            roba = await msg.Content.ReadAsStringAsync();
                        }

                        JsonObject jsonObj = JsonObject.Parse(roba);

                        String testo = jsonObj.GetNamedString("risposta");

                        tipo.Text = testo;
                    }
                    catch (Exception ex)
                    {
                        visualizza.Text = "Nope biatch!^2 " + ex.Message;
                    }
                
                }
            }            
        }

        private async void PrendiGPS(object sender, RoutedEventArgs e)
        {
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync();

                double lat = geoposition.Coordinate.Point.Position.Latitude;
                double lon = geoposition.Coordinate.Point.Position.Longitude;

                latitudine.Text =  lat.ToString("0.000000");
                longitudine.Text =  lon.ToString("0.000000");

                float Lati = (float)lat;
                float Long = (float)lon;
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x80004004)
                {
                    // the application does not have the right capability or the location master switch is off
                    longitudine.Text = "There were problems.";
                }
                else
                {
                    // something else happened acquring the location
                    string errore = ex.Message;
                }
            }
        }

        private async void cancellaAccount(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            if (localSettings.Values.ContainsKey(Voosa.getNumeroAccount()))
            {
                HttpClient elimina = new HttpClient();

                try
                {
                    HttpResponseMessage response = await elimina.GetAsync(Voosa.getServerUri() + Voosa.getEliminaAccount() + "?numero=" + localSettings.Values[Voosa.getNumeroAccount()].ToString());
                    if (response.IsSuccessStatusCode)
                    {
                        String risposta = await response.Content.ReadAsStringAsync();

                        if (risposta.Equals("Eliminazione avvenuta"))
                        {
                            App.notificaUtente("Account eliminato con successo");
                            localSettings.Values.Remove(Voosa.getNumeroAccount());
                            Frame.Navigate(typeof(CreaModificaAccount));
                        }
                        else
                        {
                            App.notificaUtente("Errore nell'eliminazione.\n" + risposta);
                        }
                    }
                    else
                    {
                        App.notificaUtente("Errore eliminazione account nel server.");
                    }
                }
                catch (Exception ex)
                {
                    App.notificaUtente("Errore: " + ex.Message);
                }

            }
        }
    }
}
