﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Data.Xml.Dom;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento per la pagina vuota è documentato all'indirizzo http://go.microsoft.com/fwlink/?LinkID=390556

namespace VoosaEye
{
    /// <summary>
    /// Pagina vuota che può essere utilizzata autonomamente oppure esplorata all'interno di un frame.
    /// </summary>
    public sealed partial class VisualizzaContatto : Page
    {
        private Contatto selezionato;
        private const int MapHeight = 389;
        private MapIcon icon;
        private ApplicationDataContainer localSettings;

        public VisualizzaContatto()
        {
            this.InitializeComponent();
            localSettings = ApplicationData.Current.LocalSettings;
        }

        /// <summary>
        /// Richiamato quando la pagina sta per essere visualizzata in un Frame.
        /// </summary>
        /// <param name="e">Dati dell'evento in cui vengono descritte le modalità con cui la pagina è stata raggiunta.
        /// Questo parametro viene in genere utilizzato per configurare la pagina.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            selezionato = e.Parameter as Contatto;

            this.DataContext = selezionato;

            if (pedinare.IsOn)
            {
                if (!selezionato.UltimaRilevazione.Equals("N/A"))
                    disegnaMappa();
                else
                    mappa.Height = 0;
            }
            else
            {
                cmdBar.Visibility = Visibility.Collapsed;
                mappa.Height = 0;
            }
        }

        private void disegnaMappa()
        {
            try
            {
                BasicGeoposition basic = new BasicGeoposition();
                basic.Latitude = selezionato.Latitudine;
                basic.Longitude = selezionato.Longitudine;

                mappa.Center = new Geopoint(basic);
                mappa.ZoomLevel = 17;

                //nel caso sia ancora nella view, elimino il pin precedentemente imposto
                if (icon == null)
                    icon = new MapIcon();
                else
                    mappa.MapElements.Remove(icon);

                icon.Title = selezionato.Nome;
                icon.Location = new Geopoint(basic);
                icon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/pin.png"));
                icon.NormalizedAnchorPoint = new Point(0.5, 1);
                mappa.MapElements.Add(icon);

                if (mappa.Height == 0)
                    mappa.Height = MapHeight;
            }
            catch (Exception ex)
            {
                App.notificaUtente("Errore: " + ex.Message);
            }
        }

        private async void OttieniPosizione(object sender, RoutedEventArgs e)
        {
            if (App.isConnectionAvailable())
            {
                HttpClient richiesta = new HttpClient(new HttpClientHandler());
                List<KeyValuePair<string, string>> dati = new List<KeyValuePair<string, string>>();

                dati.Add(new KeyValuePair<string, string>("contatto", selezionato.Numero));
                JsonObject jsonobj;
                JsonArray jsonarray;
                string array = "";

                try
                {
                    HttpResponseMessage risposta = await richiesta.PostAsync(new Uri(Voosa.getServerUri() + Voosa.getControllaPosizione()), new FormUrlEncodedContent(dati));

                    if (risposta.IsSuccessStatusCode)
                    {
                        array = await risposta.Content.ReadAsStringAsync();

                        jsonobj = JsonObject.Parse(array);
                        jsonarray = jsonobj.GetNamedArray(selezionato.Numero);

                        if (jsonarray.Count == 1)
                        {
                            //eseguo il foreach anche se ho un solo elemento
                            foreach (var item in jsonarray)
                            {
                                var itemObj = item.GetObject();
                                string data = itemObj.GetNamedString("when");

                                if (data.Equals("null"))
                                {
                                    App.notificaUtente("Non è stata rilevata nessuna posizione disponibile");
                                    return;
                                }
                                else if (selezionato.UltimaRilevazione.Equals(data))
                                {
                                    App.notificaUtente("Non è stato rilevato cambiamento nella posizone.");
                                    return;
                                }

                                string lati = itemObj.GetNamedString("lat").Replace(".", ",");
                                string longi = itemObj.GetNamedString("lng").Replace(".", ",");
                                double lat = double.Parse(lati);
                                double lng = double.Parse(longi);

                                selezionato.Latitudine = lat;
                                selezionato.Longitudine = lng;
                                selezionato.UltimaRilevazione = data;

                                if (Voosa.nellaZona(selezionato))
                                {
                                    if (!localSettings.Values.ContainsKey("interno"))
                                    {
                                        localSettings.Values["interno"] = true;

                                        ToastTemplateType toastType = ToastTemplateType.ToastText02;

                                        XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

                                        XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
                                        toastTextElement[0].AppendChild(toastXml.CreateTextNode(selezionato.Nome + " " + selezionato.UltimaRilevazione));
                                        toastTextElement[1].AppendChild(toastXml.CreateTextNode("Ha raggiunto la zona di interesse."));

                                        IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                                        ((XmlElement)toastNode).SetAttribute("duration", "long");

                                        ToastNotification toast = new ToastNotification(toastXml);
                                        ToastNotificationManager.CreateToastNotifier().Show(toast);
                                    }
                                }
                                else
                                {
                                    if (localSettings.Values.ContainsKey("interno"))
                                    {
                                        localSettings.Values.Remove("interno");

                                        ToastTemplateType toastType = ToastTemplateType.ToastText02;

                                        XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

                                        XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
                                        toastTextElement[0].AppendChild(toastXml.CreateTextNode(selezionato.Nome + " " + selezionato.UltimaRilevazione));
                                        toastTextElement[1].AppendChild(toastXml.CreateTextNode("Ha lasciato la zona di interesse."));

                                        IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                                        ((XmlElement)toastNode).SetAttribute("duration", "long");

                                        ToastNotification toast = new ToastNotification(toastXml);
                                        ToastNotificationManager.CreateToastNotifier().Show(toast);
                                    }
                                }

                                if (!DatabaseVoosa.ottieniIstanza().AggiornaContatto(selezionato))
                                {
                                    App.notificaUtente("Errore nell'aggiornamento dei dati del database.");
                                }


                                disegnaMappa();
                            }
                        }
                    }
                    else
                    {
                        App.notificaUtente("Impossibile eseguire controllo al server.\nControllare rete o riprovare più tardi.");
                    }
                }
                catch (Exception ex)
                {
                    App.notificaUtente("Errore nel recupero della posizione: " + ex.Message);
                }
            }
            else
            {
                App.notificaUtente("Connessione internet assente.\nConnettersi alla rete o riprovare più tardi.");
            }

        }

        private void emulazioneToggleEvent(object sender, TappedRoutedEventArgs e)
        {
            if (pedinare.IsOn)
            {
                if (!DatabaseVoosa.ottieniIstanza().EliminaContatto(selezionato))
                {
                    App.notificaUtente("Non è stato possibile rimuovere il contatto dal database");
                    pedinare.IsOn = true;
                    cmdBar.Visibility = Visibility.Visible;
                }
                else
                {
                    if (!selezionato.UltimaRilevazione.Equals("N/A"))
                        selezionato.UltimaRilevazione = System.DateTime.MinValue.ToString("HH:mm - dd/MM/yyyy");

                    mappa.Height = 0;
                    pedinare.IsOn = false;
                    cmdBar.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                if (!DatabaseVoosa.ottieniIstanza().AggiungiContatto(selezionato))
                {
                    App.notificaUtente("Non è stato possibile aggiungere il contatto al database");
                    pedinare.IsOn = false;
                    cmdBar.Visibility = Visibility.Collapsed;
                }
                else
                {
                    pedinare.IsOn = true;
                    cmdBar.Visibility = Visibility.Visible;
                }
            }
        }

        private void ScegliPuntoInteresse(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(PuntoInteresse), selezionato);
        }

        private void chiamaContatto(object sender, RoutedEventArgs e)
        {
            Windows.ApplicationModel.Calls.PhoneCallManager.ShowPhoneCallUI(selezionato.Numero, selezionato.Nome);
        }
    }
}