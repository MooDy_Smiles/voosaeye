<?php

	require_once 'DBConnection.php';
	require_once 'DBManager.php';

	$conn = DBConnection::getConnection();
	
	if ($conn->connect_error) {
		die("Impossibile connettersi al database.");
	}
	
	$risposta = array();
	
	foreach ($_POST as $key => $value){
		
		$result = DBManager::retrievePosition($conn, $value);
		
		if ($result->num_rows > 0){
			
			$row = $result->fetch_assoc();
			
			$lat = $row[DBConnection::DB_ACCOUNTLAT] == null ? "null" : $row[DBConnection::DB_ACCOUNTLAT];
			$lng = $row[DBConnection::DB_ACCOUNTLNG] == null ? "null" : $row[DBConnection::DB_ACCOUNTLNG];
			$ora = $row[DBConnection::DB_ACCOUNTORA] == null ? "null" : $row[DBConnection::DB_ACCOUNTORA];
			
			$nodo = array('lat'=>$lat , 'lng'=>$lng , 'when'=>$ora);
			
			//$risposta[] = array($value=>$nodo);
			$risposta[$value] = array($nodo);
		}
	}
	
	echo json_encode($risposta);

?>