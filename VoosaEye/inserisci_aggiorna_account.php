<?php
	
	require_once 'DBConnection.php';
	require_once 'DBManager.php';
	require_once 'Constants.php';

	$conn = DBConnection::getConnection();
	
	if(isset($_GET[Constants::NUMBER])) $number = $_GET[Constants::NUMBER];
	if(isset($_GET[Constants::LAT])) $lat = $_GET[Constants::LAT];
	if(isset($_GET[Constants::LNG])) $lng = $_GET[Constants::LNG];
	if(isset($_GET[Constants::WHEN])) $ora = $_GET[Constants::WHEN];

	// Check connection
	if ($conn->connect_error) {
		die("Connessione fallita: " . $conn->connect_error);
	}
	
	if(!DBManager::checkAccountTableExistence($conn)){
		
		if(!DBManager::createAccountTable($conn) === TRUE)
			die("Tabella non creata");
	}
	
	if(DBManager::checkAccountTableExistence($conn)){
		
		if(DBManager::checkAccountExistence($conn, $number)){
			
			if(isset($_GET[Constants::LAT]) && isset($_GET[Constants::LNG])){
				
				if(DBManager::updateCoordinates($conn, $number, $lat, $lng, $ora) === TRUE){
					echo "Dati aggiornati";
				} else {
					die("Dati non aggiornati ". $number ." ". $lat ." ". $lng ." ". $ora);
				}
			} else {
				
				echo "Nessun dato da aggiornare";
			}
		} else {
			
			if(isset($_GET[Constants::LAT]) && isset($_GET[Constants::LNG])){
				
				if(DBManager::addAccount($conn, $number, $lat, $lng, $ora)){
					echo "Account inserito";
				} else {
					die("Account non inserito");
				}
			} else {
				
				if(DBManager::addPartialAccount($conn, $number)){
					echo "Account inserito";
				} else {
					die("Account non inserito");
				}
			}
		}
	}

?>