<?php
	
	require_once 'DBConnection.php';
	
	class DBManager{
		
		public static function executeQuery($connection, $sql){
			
			return $connection->query($sql);
		}
		
		public static function checkAccountTableExistence($connection){
			
			$sql = "SHOW TABLES LIKE '". DBConnection::DB_ACCOUNTTABLE ."'";
			
			$result = DBManager::executeQuery($connection, $sql);
			
			if($result->num_rows > 0){
				return true;
			} else {
				return false;
			}
		}
		
		public static function createAccountTable($connection){
			
			$sql = "CREATE TABLE ". DBConnection::DB_ACCOUNTTABLE ." (
				". DBConnection::DB_ACCOUNTID ." INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
				". DBConnection::DB_CONTACTNUMBER ." CHAR(10) NOT NULL,
				". DBConnection::DB_ACCOUNTLAT ." DOUBLE(10,6),
				". DBConnection::DB_ACCOUNTLNG ." DOUBLE(10,6),
				". DBConnection::DB_ACCOUNTORA ." CHAR(16)
			)";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function addPartialAccount($connection, $number){
			
			$sql = "INSERT INTO ". DBConnection::DB_ACCOUNTTABLE ." (
			". DBConnection::DB_CONTACTNUMBER .") VALUES ($number)";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function addAccount($connection, $number, $lat, $lng, $ora){
			
			$sql = "INSERT INTO ". DBConnection::DB_ACCOUNTTABLE ." (
			". DBConnection::DB_CONTACTNUMBER .", ". DBConnection::DB_ACCOUNTLAT .", ". DBConnection::DB_ACCOUNTLNG .", ". DBConnection::DB_ACCOUNTORA .") VALUES (
			$number , $lat, $lng, '$ora' )";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function removeAccount($connection, $number){
			
			$sql = "DELETE FROM ". DBConnection::DB_ACCOUNTTABLE ." WHERE "
			. DBConnection::DB_CONTACTNUMBER ." = $number ";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function updateCoordinates($connection, $number, $lat, $lng, $ora){
			
			$sql = "UPDATE ". DBConnection::DB_ACCOUNTTABLE ." SET ".
			DBConnection::DB_ACCOUNTLAT ." =  $lat , ". DBConnection::DB_ACCOUNTLNG ." = $lng , ". DBConnection::DB_ACCOUNTORA ." = '$ora' WHERE ".
			DBConnection::DB_CONTACTNUMBER ." = $number";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function retrievePosition($connection, $number){
			
			$sql = "SELECT ". DBConnection::DB_ACCOUNTLAT .", ". DBConnection::DB_ACCOUNTLNG .", ". DBConnection::DB_ACCOUNTORA
			." FROM ". DBConnection::DB_ACCOUNTTABLE ." WHERE ". DBConnection::DB_CONTACTNUMBER ." = $number";
			
			return DBManager::executeQuery($connection, $sql);
		}
		
		public static function checkAccountExistence($connection, $number){
			
			$sql = "SELECT * FROM ". DBConnection::DB_ACCOUNTTABLE ." WHERE ". DBConnection::DB_CONTACTNUMBER ." = $number";
			
			$result = DBManager::executeQuery($connection, $sql);
			
			if($result->num_rows > 0){
				return true;
			} else {
				return false;
			}
		}
		
	}

?>