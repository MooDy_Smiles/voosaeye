<?php
	
	require_once 'DBConnection.php';
	require_once 'DBManager.php';

	$conn = DBConnection::getConnection();
	
	if ($conn->connect_error) {
		die("Impossibile connettersi al database.");
	}
	
	$risultato = "";
	
	foreach ($_POST as $key => $value) {
		
		if (DBManager::checkAccountExistence($conn, $value)){
			
			$risultato = $risultato . $key .";";
		}
	}
	
	if (strlen($risultato) > 0){
		
		echo substr($risultato, 0, -1);
	} else {
		echo "Nessun risultato da restituire";
	}
?>