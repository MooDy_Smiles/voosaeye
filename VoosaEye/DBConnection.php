<?php
		
	/**
	 *	Classe per la creazione/distruzione della connessione con il database DB_VOOSAEYE
	 */
	class DBConnection{
		
		const SERVER_NAME = 'localhost';
		const DB_USERNAME = 'root';
		const DB_PASSWORD = '';
		const DB_VOOSAEYE = 'test';
		
		/**	
		 *	Costanti per la tabella degli account
		 */
		const DB_ACCOUNTTABLE = 'UsersAccounts';
		const DB_ACCOUNTID = 'id';
		const DB_CONTACTNUMBER = 'contactNumber';
		const DB_ACCOUNTLAT = 'lat';
		const DB_ACCOUNTLNG = 'lng';
		const DB_ACCOUNTORA = 'lastdetect';
		
		public static function getConnection(){
			
			return new mysqli(DBConnection::SERVER_NAME, DBConnection::DB_USERNAME, DBConnection::DB_PASSWORD, DBConnection::DB_VOOSAEYE);
		}
		
		public static function releaseConnection($connection){
			
			if($connection){
				$connection->close();
			}
		}
	}

?>