﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace BackgroundTask
{
    public sealed class TaskTest : IBackgroundTask
    {
        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            var def = taskInstance.GetDeferral();

            var result = await BackgroundExecutionManager.RequestAccessAsync();

            HttpClient client = new HttpClient();

            HttpResponseMessage response = await client.GetAsync("http://time.jsontest.com/");

            String content = await response.Content.ReadAsStringAsync();

            Debug.WriteLine(content);

            //prova con il toast

            ToastTemplateType toastType = ToastTemplateType.ToastText02;

            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

            XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
            toastTextElement[0].AppendChild(toastXml.CreateTextNode("Ciao Umano"));
            toastTextElement[1].AppendChild(toastXml.CreateTextNode("Notifica delle " + DateTime.Now.ToString("HH:mm - dd/MM/yyyy")));

            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((XmlElement)toastNode).SetAttribute("duration", "long");

            ToastNotification toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);  

            //notify task is complete
            def.Complete();
        }
    }
}
