﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using VoosaEye;
using Windows.Data.Json;
using Windows.UI.Popups;
using Windows.Devices.Geolocation;
using Windows.Storage;

namespace BackgroundTask
{
    public sealed class OfficialTask : IBackgroundTask
    {
        private List<Contatto> pedinare;
        private List<KeyValuePair<string, string>> form;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            var def = taskInstance.GetDeferral();

            var result = await BackgroundExecutionManager.RequestAccessAsync();

            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            //se non c'è l'account, non eseguo le operazioni di routine
            if (!localSettings.Values.ContainsKey(Voosa.getNumeroAccount())) return;

            //Preparo la lista di contatti da controllare sul server
            pedinare = DatabaseVoosa.ottieniIstanza().RecuperaContatti();

            form = new List<KeyValuePair<string, string>>();

            int i = 1;

            foreach (Contatto contatto in pedinare)
            {
                form.Add(new KeyValuePair<string, string>(i.ToString(), contatto.Numero));
                i++;
            }

            //eseguo l'invio della richiesta e recupero della risposta
            string array = "";

            HttpClient richiesta = new HttpClient(new HttpClientHandler());

            try
            {
                HttpResponseMessage risposta = await richiesta.PostAsync(new Uri(Voosa.getServerUri() + Voosa.getControllaPosizione()), new FormUrlEncodedContent(form));
                if (risposta.IsSuccessStatusCode)
                {
                    array = await risposta.Content.ReadAsStringAsync();
                    
                    JsonObject json = JsonObject.Parse(array);

                    foreach (Contatto contatto in pedinare)
                    {
                        JsonArray jarray = json.GetNamedArray(contatto.Numero);

                        foreach (var item in jarray)
                        {
                            var itemObj = item.GetObject();

                            string data = itemObj.GetNamedString("when");

                            if (!data.Equals("null") && !contatto.UltimaRilevazione.Equals(data))
                            {
                                string lati = itemObj.GetNamedString("lat").Replace(".", ",");
                                string longi = itemObj.GetNamedString("lng").Replace(".", ",");
                                double lat = double.Parse(lati);
                                double lng = double.Parse(longi);

                                contatto.Latitudine = lat;
                                contatto.Longitudine = lng;
                                contatto.UltimaRilevazione = data;

                                if (Voosa.nellaZona(contatto))
                                {
                                    ToastTemplateType toastType = ToastTemplateType.ToastText02;

                                    XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

                                    XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
                                    toastTextElement[0].AppendChild(toastXml.CreateTextNode(contatto.Nome));
                                    toastTextElement[1].AppendChild(toastXml.CreateTextNode("Ha raggiunto la zona di interesse alle " + contatto.UltimaRilevazione));

                                    IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                                    ((XmlElement)toastNode).SetAttribute("duration", "long");

                                    ToastNotification toast = new ToastNotification(toastXml);
                                    ToastNotificationManager.CreateToastNotifier().Show(toast);
                                }

                                DatabaseVoosa.ottieniIstanza().AggiornaContatto(contatto);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                array = ex.Message;
            }

            //ottengo la posizione dell'utente da inviare al server

            if (localSettings.Values.ContainsKey(Voosa.getUsoPosizione()) && !(bool)localSettings.Values[Voosa.getUsoPosizione()]) return;

            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;

            try
            {
                Geoposition geoposition = await geolocator.GetGeopositionAsync();

                double lat = geoposition.Coordinate.Point.Position.Latitude;
                double lon = geoposition.Coordinate.Point.Position.Longitude;

                string latValue = lat.ToString("0.000000").Replace(",",".");
                string lonValue = lon.ToString("0.000000").Replace(",", ".");
                string dataRilevazione = DateTime.Now.ToString("HH:mm - dd/MM/yyyy");

                HttpClient inviaPosizione = new HttpClient(new HttpClientHandler());

                HttpResponseMessage risposta = await inviaPosizione.GetAsync(new Uri(Voosa.getServerUri() + Voosa.getAggiungiAggiornaAccount() + "?numero=" + localSettings.Values[Voosa.getNumeroAccount()]
                    + "&latitudine=" + latValue + "&longitudine=" + lonValue + "&orario=" + dataRilevazione));

            }
            catch (Exception ex)
            { 
            
            }

            /*ToastTemplateType toastType = ToastTemplateType.ToastText02;

            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastType);

            XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
            toastTextElement[0].AppendChild(toastXml.CreateTextNode("Ciao Umano"));
            toastTextElement[1].AppendChild(toastXml.CreateTextNode("Notifica delle " + DateTime.Now.ToString("HH:mm - dd/MM/yyyy")));

            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((XmlElement)toastNode).SetAttribute("duration", "long");

            ToastNotification toast = new ToastNotification(toastXml);
            ToastNotificationManager.CreateToastNotifier().Show(toast);*/

            //notify task is complete
            def.Complete();
        }
    }
}
